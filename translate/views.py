import redis
from django.http import HttpResponse
from django.utils.translation import get_language_from_request
from redis_translate.reddis import GetDictionary
from django.conf import settings

if settings.DEBUG:
    r = redis.StrictRedis(host=settings.REDIS_HOST_DEBUG, port=settings.REDIS_PORT, db=0)
else:
    r = redis.StrictRedis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=0)


def welcome_view(request):
    output = "Welcome to my site"
    lang = get_language_from_request(request, check_path=True)
    output = GetDictionary.get_dictionary(r, output, lang=lang)
    return HttpResponse(output)


def your_lang(request):
    output = "It's seems like your language is "
    lang = get_language_from_request(request, check_path=True)
    output = GetDictionary.get_dictionary(r, output, lang=lang)
    output += lang
    return HttpResponse(output)


def one_more_phrase(request):
    output = "One more phrase"
    lang = get_language_from_request(request, check_path=True)
    output = GetDictionary.get_dictionary(r, output, lang=lang)
    return HttpResponse(output)


def is_deploy_workin(request):
    output = "If you see this, everything's good"
    return HttpResponse(output)
