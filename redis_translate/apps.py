from django.apps import AppConfig


class RedisTranslateConfig(AppConfig):
    name = 'redis_translate'
