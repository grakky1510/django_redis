class WriteDictionary:

    def __init__(self, redis_, phrase, *translate_dicts):
        with redis_.pipeline() as pipe:
            for lang_phrase in translate_dicts:
                pipe.hset(phrase, mapping=lang_phrase)
            pipe.execute()


class GetDictionary:
    @staticmethod
    def get_dictionary(redis_, phrase, lang="en"):
        translated = redis_.hget(phrase, lang)
        if translated:
            return translated.decode("utf-8")
        else:
            default_phrase = redis_.hget(phrase, "en")
            if default_phrase:
                return default_phrase.decode("utf-8")
            else:
                raise KeyError("No such phrase in dictionary. Add your phrase in dictionary.py file.")

