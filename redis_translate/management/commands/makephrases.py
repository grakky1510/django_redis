import redis
from django.conf import settings
from redis_translate.reddis import WriteDictionary
from redis_translate.dictionary import list_of_phrases
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--debug',
            action='store_true',
            help='Run in debug mode',
        )

    def handle(self, *args, **options):
        if options['debug']:
            r = redis.StrictRedis(host=settings.REDIS_HOST_DEBUG, port=settings.REDIS_PORT, db=0)
        else:
            r = redis.StrictRedis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=0)
        for phrase in list_of_phrases:
            WriteDictionary(r, *phrase)
