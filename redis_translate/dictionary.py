list_of_phrases = [
    (
        "Welcome to my site", {
            "ru": "Добро пожаловать на мой сайт",
            "en": "Welcome to my site",
            "de": "Willkommen auf meiner Seite"
        }
    ),
    (
        "It's seems like your language is ", {
            "ru": "Похоже твой язык это ",
            "en": "It's seems like your language is ",
            "de": "Anscheinend ist deine Sprache "
        }
    ),
    (
        "One more phrase", {
            "ru": "Ещё одна фраза",
            "en": "One more phrase",
            "de": "Noch ein Satz"
        }
    ),

]
