from django.test import TestCase
from django.urls import reverse
from django.conf import settings


class TestLanguage(TestCase):

    def setUp(self):
        settings.DEBUG = True

    def test_russian(self):
        headers = {"HTTP_ACCEPT_LANGUAGE": "ru"}
        url = reverse("welcome")
        response = self.client.get(url, **headers)
        result = response.content.decode("utf-8")
        self.assertEqual(result, "Добро пожаловать на мой сайт")

    def test_default(self):
        url = reverse("welcome")
        response = self.client.get(url)
        result = response.content.decode("utf-8")
        self.assertEqual(result, "Welcome to my site")

    def test_english(self):
        headers = {"HTTP_ACCEPT_LANGUAGE": "en"}
        url = reverse("welcome")
        response = self.client.get(url, **headers)
        result = response.content.decode("utf-8")
        self.assertEqual(result, "Welcome to my site")

    def test_check_lang(self):
        headers = {"HTTP_ACCEPT_LANGUAGE": "ru"}
        url = reverse("your lang")
        response = self.client.get(url, **headers)
        result = response.content.decode("utf-8")
        self.assertEqual(result, "Похоже твой язык это ru")

